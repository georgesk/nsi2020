Un dépôt pour les activités de NSI
==================================

Le dépôt ici est pour les activités de projet de l'année scolaire 
2019-2020. Chaque sous-répertoire, à ce niveau, est l'affaire d'une équipe
d'élèves. Les participants s'engagent à ne pas modifier les fichiers d'une
équipe tierce sans son consentement ; chacun travaille de préférence 
« chez soi »..

Pourquoi utiliser Framagit ?
----------------------------

Framagit est un service libre, fondé sur des logiciels libres, et avec
une volonté éthique claire : on ne cherche pas à exploiter les données de
vie privée des utilisateurs.

Framagit repose sur le logiciel libre Git 
(voir sa page [wikipedia](https://fr.wikipedia.org/wiki/Git)).
Git a été conçu, de base, pour permettre à de grandes équipes de développeurs
de travailler sur des projets communs, avec comme point majeur la
sécurité de l'information : 

- **tout fichier géré par Git pourra être retrouvé**, dans chacune des versions
qui ont été `commises`.
- il est possible le même projet dans diverses **branches indépendantes**
- des échanges de données sont possibles d'une branche à l'autre

Comment on récupère un projet présent dans Framagit :
-----------------------------------------------------

Il y a deux méthodes : la méthode naïve d'exportation, et la méthode
native de « clonage Git ».

### Méthode naïve, l'exportation
1. on déroule le menu des branches, qui contient `master` par défaut, mais
   aussi une branche `prof` et peut-être d'autres branches si on les crée.
   On choisit une des branches : plutôt `master` pour la version des élèves,
   plutôt `prof` pour les modifications et/ou suggestions apportées par
   le professeur.
2. on clique sur l'icône de téléchargement, qui se trouve juste à gauche
   du bouton bleu `Clone`. Puis on clique sur le type d'archive qu'on préfère
   récupérer : `zip`, `tar.gz`, etc. ; ça provoque le téléchargement d'une
   archive contenant tous les fichiers du dépôt, et on peut alors travailler.
   
### Méthode native, clonage Git
1. on clique directement sur le bouton `Clone`. Dans ce cas, on voit apparaître
   deux URLs, une pour le clonage de type https, l'autre pour le clonage de
   type ssh. Quand on débute, on peut mettre l'URL du clonage https dans le
   presse-papier
2. on ouvre un terminal (sous Linux) ou une fenêtre git-bash (sous Windows).
   Dans cette nouvelle fenêtre, on tape la commande `git` puis un espace, puis
   l'URL de clonage ; enfin, on valide avec la touche Entrée.
   
Avantages, inconvénients des méthodes naïve et native
-----------------------------------------------------

La **méthode naïve** permet de récupérer un arbre de fichier de façon 
traditionnelle, on déplie l'archive quelque part dans le système de 
fichiers et on travaille « à l'ordinaire », sans besoin d'apprendre 
du nouveau. **Inconvénient** : les fichiers récupérés sont datés du
jour du téléchargement, mais il n'y a pas de suivi de version. En dehors
de votre mémoire, rien ne peut vous indiquer l'historique de tel ou tel
fichier s'il a été modifié ni quand. De plus, il faut choisir une branche
et une seule : on peut avoir la branche  `master` ou la branche `prof`,
mais pas les deux en même temps. Pour avoir plus d'une branche il faut
recommencer l'opération plusieurs fois et déplier les archives dans des
zones différentes du système de fichiers.

La **méthode native** fait appel à Git, avec la philosophe qui va avec :
tout fichier géré par Git pourra être retrouvé, il y a des branches
indépendantes, etc. Avec un seul téléchargement, on a toutes les branches
à la fois, et on peut passer de l'une à l'autre à l'aide de la commande
en ligne `git checkout`. On peut tout savoir concernant les versions des
fichiers à l'aide de la commande en ligne `git log`. **Inconvénient** : 
il faut prendre le temps d'apprivoiser Git ; c'est comme de conduire un
véhicule performant, ça demande un peu de persévérance pour bénéficier de
toute la puissance. 

Celles et ceux qui ont appris Git pour développer
ne l'ont jamais regretté, et c'est une ligne qu'on peut mettre sur un
*curriculum vitae*.

INSTALLATION de Git
===================

Sous Linux
----------

Soit il est *déjà installé* (c'est le cas pour les clés Freeduc), soit
on l'installe : pour une distribution Debian ou Ubuntu, c'est
la commande en ligne `sudo apt install git`.

Sous Windows
------------

Installer la "version portable" de Git-Scm, qui va bien pour votre machine.
Cherchez "Git for Windows Portable" dans
[cette page web du site officiel](https://git-scm.com/download/win).
