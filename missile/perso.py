import pygame
from missile import Missile
from collections import namedtuple



class joueur(pygame.sprite.Sprite):
    def __init__(self):
        pygame.sprite.Sprite.__init__(self)
        self.pv = 100
        self.max_pv = 100
        self.vitesse = 10
        self.attaque = 10
        self.tout_les_missiles = pygame.sprite.Group()
        self.texture = pygame.image.load("perso.png").convert_alpha()
        self.hitbox = self.texture.get_rect()
        self.hitbox.x = 10
        self.hitbox.y = 820
        self.miroir_gauche = True
        self.miroir_droite = False
        self.retourné_gauche = False                 #tourné vers la gauche
        self.retourné_droite = True                 #tourné vers la droite
        

    def lancer_missile(self, fenetre, position, angle, vitesse):
        missile = Missile(fenetre.fenetre, position, angle,100)
        self.tout_les_missiles.add(missile)
        if missile.rect.x > 1920 or missile.rect.x < 0:
            self.tout_les_missiles.remove(self)

    def aller_a_droite(self):
        self.hitbox.x += self.vitesse
        if self.retourné_gauche == True:
            self.miroir_droite = True
            if self.miroir_droite == True:
                self.texture = pygame.transform.flip(self.texture, True, False)
                self.miroir_droite = False
                self.retourné_droite = True
                self.retourné_gauche = False

            
    def aller_a_gauche(self):
        self.hitbox.x -= self.vitesse
        if self.retourné_droite == True:
            self.miroir_gauche = True
            if self.miroir_gauche == True:
                self.texture = pygame.transform.flip(self.texture, True, False)
                self.miroir_gauche = False
                self.retourné_gauche = True
                self.retourné_droite = False