JEU 2D de lancer de missiles
============================

Auteurs : 
---------
- SWERTVAEGHER Clémentine
- VERLANDE Quentin
- VINEL Adrien


Interventions du prof :
-----------------------

12/04/2020 : « fait la physique des missiles ».

Une façon de calculer la position en temps réel d'un missile après son lacement
a été mise au point, et une fonction de démonstration a été écrite. Pour voir
la démonstration : `python3 missile.py`
