import pygame, time, math
from collections import namedtuple

# définition du type Vecteur
Vecteur = namedtuple("Vecteur", ("x", "y"))

class Missile(pygame.sprite.Sprite):

    """
    Arguments du constructeur :

    :param fenetre: la fenêtre de l'application pygame, afin de pouvoir
      y remettre les choses à l'endroit (retournement de l'axe y, qui va
      vers le bas pour pygame, et qui doit être vers le haut, pour que
      la trigonométrie habituelle fonctionne)
    :type fenetre: pygame.Surface
    :param position: un vecteur position, en unité pixel
    :type  position: Vecteur
    :param angle: un angle en degré, compté comme pour la trigonométrie
    :type  angle: float
    :param vitesse: une vitesse scalaire en mètre par seconde
    :type  vitesse: float
    :param delai: un délai en secondes après lequel le missile est lancé.
      Le délai est nul par défaut
    :param image: le nom d'un fichier d'image, "missile.png" par défaut
    :type  image: str
    :param centre: un vecteur en pixels, qui dit où est le centre de
      l'image, pour les calculs de balistique. Si ce paramètre est None
      (par défaut) alors ce centre est juste le centre du rectangle de
      l'image, obtenu en divisant largeur et hauteur par deux.
    :type centre: Vecteur or None
    """

    gravite  = 9.8 # la gravité terrestre
    echelle  = 10  # échelle 10 pixels par mètre
    # avec une telle échelle, le moindre missile est « géant »
    
    def __init__(self, fenetre, position, angle, vitesse,
                 delai=0, image="missile.png", centre=None):
        super().__init__()
        self.fenetre      = fenetre
        self.angle        = angle
        self.delai        = delai
        self.image        = pygame.image.load(image).convert_alpha()
        if centre:
            self.centre = centre
        else:
            self.centre = Vecteur(
                x = int(self.image.get_width()/2),
                y = int(self.image.get_height()/2),
            )
        self.rect_initial = self.image.get_rect().move(
            - self.centre.x,
            self.fenetre.get_height() - self.centre.y
        )
        self.rect   = self.rect_initial.move(0, 0)
        # les données qui suivent sont en système international (mètre, seconde)
        self.date_lancement    = time.time() + delai
        self.position_initiale = Vecteur(
            x = position.x/self.echelle,
            y = position.y/self.echelle
        )
        self.vitesse_initiale  = Vecteur(
            x = vitesse * math.cos(angle*math.pi/180),
            y = vitesse * math.sin(angle*math.pi/180),
        )

    def __str__(self):
        """
        Transformation en chaîne de caractères
        """
        return f"Missile : position = {self.rect}"

    def bouge(self):
        """
        permet de savoir en temps réel la position du missile, et modifie
        par effet de bord self.rect

        :return: une position en px ; si le délai n'est pas encore
          dépassé renvoie la position initiale à l'écran
        :rtype: Vecteur

        """
        t = time.time() - self.date_lancement
        if t < 0:
            t = 0 # (on ne bouge pas encore avant le lancement)
        # calcul selon la loi de la chute libre, voir cours de
        # terminale en physique
        x = self.position_initiale.x + self.vitesse_initiale.x * t
        y = self.position_initiale.y + self.vitesse_initiale.y * t \
            - self.gravite * t**2
        # conversion en pixels et retourment de l'axe y.
        nouvelle_position =  Vecteur(
            int(x * self.echelle),
            - int(y * self.echelle)
        )
        self.rect = self.rect_initial.move(*nouvelle_position)
        return nouvelle_position

    def direction(self):
        """
        permet de savoir en temps réel la direction du missile,
        c'est à dire l'angle que forme le vecteur vitesse avec
        l'origine trigonométrique des directions. Ça peut servir pour
        tourner l'image en cours de trajectoire.

        :return: un angle en degré
        :rtype: float

        """
        t = time.time() - self.date_lancement
        if t < 0:
            t = 0 # (on ne bouge pas encore avant le lancement)
        # calcul de vitesse selon la loi de la chute libre, voir cours de
        # terminale en physique
        vx = self.vitesse_initiale.x
        vy = self.vitesse_initiale.y - self.gravite * t
        return math.atan2(vy, vx) * 180 / math.pi
        

def demo():
    """
    Une courte démonstration de deux objets Missiles :

    On lance le premier depuis la position (200,400)
    avec un angle de 45°, une vitesse de 20 m/s,
    après un délai d'un seconde qui suit l'ouverture de la fenêtre

    ...
    et un deuxième depuis la position (600,400), avec un angle de 135°,
    une vitesse de 25 m/s, après un délai
    d'une seconde et demi après ouverture de la fenêtre
    """
    largeur = 800
    hauteur = 600

    fenetre = pygame.display.set_mode((largeur, hauteur))
    pygame.display.set_caption("Démonstration de missile")
    missile1 = Missile(
        fenetre,
        Vecteur(200,400), # position initiale en px
        45,               # angle 45 degrés
        20,               # vitesse initale 20 m/s
        1,                # délai une seconde
        # les paramètres image et centre sont "par défault"
    )
    missile2 = Missile(
        fenetre,
        Vecteur(600,400), # position initiale en px
        135,              # angle 90 + 45 degrés
        25,               # vitesse initale 25 m/s
        1.5,              # délai une seconde et demi
        # les paramètres image et centre sont "par défault"
    )

    groupe_missiles = pygame.sprite.Group()
    groupe_missiles.add(missile1)
    groupe_missiles.add(missile2)
    continuer = True
    white = pygame.Color(255,255,255)
    while continuer:
        fenetre.fill(white)
        for missile in groupe_missiles.sprites():
            missile.bouge()
        groupe_missiles.draw(fenetre)
        pygame.display.flip()
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                continuer = False
    pygame.quit()
          
if __name__ == "__main__":
    demo()
