import pygame
from pygame.locals import*
import perso

class Fen:
    """
    Paramètres du constructeur :


    :param longueur: la largeur de la fenêtre (1920 px par défaut)
    :param largeur: la hauteur de la fenêtre (1080 px par défaut)
    :param fond: le nom d'un fichier d'image, "fond.png" par défaut
    :param titre: un titre pour la fenêtre, "Epic Bordel Simulator" par défaut
    """
    
    def __init__( self, longueur = 4300, largeur = 1080,
                 fond = "fond.png", titre = "Epic Bordel Simulator"):
        self.longueur = longueur
        self.largeur = largeur
        self.fenetre = pygame.display.set_mode((longueur, largeur))
        pygame.display.set_caption(titre)
        pygame.key.set_repeat(1, 1)
        self.fond = pygame.image.load(fond).convert()
        #self.fond = pygame.transform.scale(self.fond, (longueur, largeur))

    def blit(self, surface, position):
        """
        Renvoie les arguments à self.fenetre (c'est un simple proxy)

        :param surface: une image à afficher
        :type surface: pygame.Surface
        :param position: une position
        """
        self.fenetre.blit(surface, position)
