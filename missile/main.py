    ## imports ##
import pygame
from pygame.locals import*
import perso
import Fen
from jeu import Jeu
from missile import Vecteur
import math


def run():
    """
    L'entrée du programme principal
    """
    continuer = 1
    pygame.init()
    fenetre = Fen.Fen(longueur=1920, largeur=1080)
    jeu = Jeu()
    coordo_x_fond = 0
    angle_tir = 0

    #quelques textures
    viseur = pygame.image.load("viseur.png").convert_alpha()

    ## jeu ##
    while continuer:

        #on cache la souris
        pygame.mouse.set_visible(False)

        #on affiche les élément à une position donnée

        fenetre.blit(fenetre.fond, (coordo_x_fond, 0))
        fenetre.blit(jeu.joueur.texture, jeu.joueur.hitbox)
        # on gère le mouvement des missiles
        for missile in jeu.joueur.tout_les_missiles.sprites():
            missile.bouge()
        jeu.joueur.tout_les_missiles.draw(fenetre)

        #on affiche une croix pour viser 
        souris_x = pygame.mouse.get_pos()[0]
        souris_y = pygame.mouse.get_pos()[1]
        fenetre.blit(viseur, (souris_x, souris_y))

        #on raffraichi l'écran
        pygame.display.flip()

        #on recherche les différents évenements qui se produisent dans la boucle

            #on vérifie quelles touches sont pressées
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                continuer = 0
                pygame.quit()

            elif event.type == pygame.KEYDOWN:
                jeu.pressed[event.key] = True

                if event.key == pygame.K_SPACE:
                    jeu.joueur.lancer_missile(fenetre, Vecteur(jeu.joueur.hitbox.x, jeu.joueur.hitbox.y - 650), angle_tir, 40)

            elif event.type == pygame.KEYUP:
                jeu.pressed[event.key] = False

                if event.key == K_F5:
                    pygame.display.toggle_fullscreen()
                


            #on déplace le joueur
        if jeu.pressed.get(pygame.K_d) and jeu.joueur.hitbox.x + jeu.joueur.hitbox.width < fenetre.longueur:
            jeu.joueur.aller_a_droite()
            coordo_x_fond -= 5

        if jeu.pressed.get(pygame.K_q) and jeu.joueur.hitbox.x > 0:
            jeu.joueur.aller_a_gauche()
            coordo_x_fond += 5

        BC = jeu.joueur.hitbox.y - souris_y + 40
        AC = souris_x + 40 -jeu.joueur.hitbox.x
        if AC != 0:
            angle_radian = math.tan(BC/AC)
            angle_tir = math.degrees(angle_radian)
        else:
            angle_tir = 90        
if __name__ == "__main__":
    run()
